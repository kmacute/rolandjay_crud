import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CategoryList from './Pages/Categories/CategoryList';
import CatogoryAddEdit from './Pages/Categories/CatogoryAddEdit';
import CatogoryView from './Pages/Categories/CatogoryView';

function App () {
	return (
		<Router>
			<div className='container-fluid'>
				<Switch>
					<Route path='/categories' exact>
						<CategoryList />
					</Route>
					<Route path='/categories/create' exact>
						<CatogoryAddEdit />
					</Route>
					<Route path='/categories/:id/edit' exact>
						<CatogoryAddEdit />
					</Route>
					<Route path='/categories/:id' exact>
						<CatogoryView />
					</Route>
				</Switch>
			</div>
		</Router>
	);
}

export default App;
