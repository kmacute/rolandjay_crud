import { useLazyQuery, useMutation } from '@apollo/client';
import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { categoryFetchByIdQuery, categoryInsertQuery, categoryUpdateQuery } from './components/CategoriesQueries';

interface IParams {
	id: string;
}

interface IData {
	id: string;
	category_name: string;
}

const CatogoryAddEdit = () => {
	const { id } = useParams<IParams>();
	const history = useHistory();
	const [ data, setData ] = useState<IData>({
		id: '',
		category_name: ''
	});

	const [ categoryInsert ] = useMutation(categoryInsertQuery);
	const [ categoryUpdate ] = useMutation(categoryUpdateQuery);
	const [ categoryFetchById, { loading, error, data: editData } ] = useLazyQuery(categoryFetchByIdQuery);

	useEffect(
		() => {
			if (id) {
				fetchData();
			}
		},
		[ id ]
	);

	useEffect(
		() => {
			if (editData) {
				setData(editData.categories[0]);
			}
		},
		[ editData ]
	);

	const handleChange = (e: any) => {
		const { name, value } = e.target;

		setData((prev) => ({
			...prev,
			[name]: value
		}));
	};

	const backHandleClick = () => {
		history.goBack();
	};

	const handleSubmit: React.FormEventHandler<HTMLFormElement> = (e) => {
		e.preventDefault();

		// kung may laman
		if (id) {
			EditData();
		} else {
			AddData();
		}
	};

	const AddData = async () => {
		await categoryInsert({
			variables: {
				category_name: data.category_name
			}
		});

		history.goBack();
	};

	const EditData = async () => {
		await categoryUpdate({
			variables: {
				category_name: data.category_name,
				id: id
			}
		});

		history.goBack();
	};

	const fetchData = async () => {
		const result = await categoryFetchById({
			variables: {
				id: id
			}
		});

		console.log(result);
	};

	if (error) return <div>Something went wrong</div>;
	if (loading)
		return (
			<div>
				<i className='fas fa-spinner fa-spin' /> Loading
			</div>
		);

	return (
		<div>
			<h1>{id ? 'Edit' : 'Add'} Category</h1>
			<button className='btn btn-info' onClick={backHandleClick}>
				Back
			</button>
			<div className='row'>
				<div className='col-md-8'>
					<div className='card'>
						<div className='card-body'>
							<form onSubmit={handleSubmit}>
								<div className='mb-2'>
									<label className='label-control'>Category</label>
									<input
										type='text'
										className='form-control'
										required
										name='category_name'
										value={data.category_name}
										onChange={handleChange}
									/>
								</div>

								<button className='btn btn-primary'>Save</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CatogoryAddEdit;
