import { useMutation, useSubscription } from '@apollo/client';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { categoryDeleteQuery, categoryListQuery } from './components/CategoriesQueries';

export interface ICategory {
	id?: string;
	category_name: string;
	created_at?: Date;
	updated_at?: Date;
}

const CategoryList = () => {
	const [ categoryDelete ] = useMutation(categoryDeleteQuery);
	const { error, loading, data } = useSubscription(categoryListQuery);
	const history = useHistory();

	if (error) return <div>Something went wrong</div>;
	if (loading)
		return (
			<div>
				<i className='fas fa-spinner fa-spin' /> Loading
			</div>
		);

	const addHandleClick = () => {
		history.push('/categories/create');
	};

	const editHandleClick = (id: any) => {
		history.push(`/categories/${id}/edit`);
	};

	const deletehandleClick = (id: any) => {
		if (confirm(`Are you sure you want to delete this record ${id}`)) {
			categoryDelete({
				variables: {
					id: id
				}
			});
		}
	};

	return (
		<div>
			<h1>Category List</h1>
			<button className='btn btn-info' onClick={addHandleClick}>
				Add
			</button>
			<hr />
			<table className='table '>
				<thead>
					<tr>
						<th>id</th>
						<th>Description</th>
						<th>Created At</th>
						<th>Updated At</th>
						<th>Option</th>
					</tr>
				</thead>
				<tbody>
					{data.categories.map((row: ICategory, key: number) => (
						<tr key={key}>
							<td>{row.id}</td>
							<td>{row.category_name}</td>
							<td>{row.created_at}</td>
							<td>{row.updated_at}</td>
							<td>
								<button className='btn btn-success' onClick={() => editHandleClick(row.id)}>
									Edit
								</button>
								<button className='btn btn-danger' onClick={() => deletehandleClick(row.id)}>
									<i className='fas fa-trash' />
								</button>
							</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
};

export default CategoryList;
