import React from 'react';
import { useParams } from 'react-router-dom';

interface IParams {
	id: string;
}

const CatogoryView = () => {
	const { id } = useParams<IParams>();

	return (
		<div>
			{id}
			<h1>View</h1>
		</div>
	);
};

export default CatogoryView;
