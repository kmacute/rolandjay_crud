import { gql } from '@apollo/client';

export const categoryListQuery = gql`
	subscription {
		categories {
			id
			category_name
			created_at
			updated_at
		}
	}
`;

export const categoryInsertQuery = gql`
	mutation categoryInsert($category_name: String) {
		insert_categories(objects: { category_name: $category_name }) {
			affected_rows
		}
	}
`;

export const categoryFetchByIdQuery = gql`
	query categoryFetchById($id: uuid = "") {
		categories(where: { id: { _eq: $id } }) {
			category_name
			id
		}
	}
`;

export const categoryUpdateQuery = gql`
	mutation categoryUpdate($id: uuid = "", $category_name: String = "") {
		update_categories(where: { id: { _eq: $id } }, _set: { category_name: $category_name }) {
			affected_rows
		}
	}
`;

export const categoryDeleteQuery = gql`
	mutation categoryDelete($id: uuid = "") {
		delete_categories(where: { id: { _eq: $id } }) {
			affected_rows
		}
	}
`;
