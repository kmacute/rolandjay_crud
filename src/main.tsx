import { ApolloClient, ApolloProvider, HttpLink, InMemoryCache, split } from '@apollo/client';
import { getMainDefinition } from '@apollo/client/utilities';
import { WebSocketLink } from '@apollo/link-ws';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

const GRAPHQL_ENDPOINT = 'localhost:8080/v1/graphql';

const httpLink = new HttpLink({
	uri: `http://${GRAPHQL_ENDPOINT}`
});

const wsLink = new WebSocketLink({
	uri: `ws://${GRAPHQL_ENDPOINT}`,
	options: {
		reconnect: true
	}
});

const splitLink = split(
	({ query }) => {
		const definition = getMainDefinition(query);
		return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
	},
	wsLink,
	httpLink
);

const client = new ApolloClient({
	cache: new InMemoryCache(),
	link: splitLink
});

ReactDOM.render(
	<React.StrictMode>
		<ApolloProvider client={client}>
			<App />
		</ApolloProvider>
	</React.StrictMode>,
	document.getElementById('root')
);
